CC =	cc
CFLAGS =	-O2 -pipe -fstack-protector -fno-strict-aliasing

PREFIX	=	/usr/local
BINDIR =	/bin
CONFDIR =	/etc/bq
SHAREDIR =	/share/bq

all:
	${CC} ${CFLAGS} -Wall -o bq bq.c

install: all
	install -d -m 755 ${PREFIX}${BINDIR}
	install -d -m 755 ${PREFIX}${CONFDIR}
	install -d -m 755 ${PREFIX}${SHAREDIR}
	install -m 755 bq ${PREFIX}${BINDIR}
	install -m 755 bq.trigger.n0 ${PREFIX}${CONFDIR}
	install -m 755 bq.trigger.n1 ${PREFIX}${CONFDIR}
	install -m 755 README.txt ${PREFIX}${SHAREDIR}
	install -m 755 CHANGELOG.txt ${PREFIX}${SHAREDIR}

deinstall:
	rm ${PREFIX}${BINDIR}/bq
	rm ${PREFIX}${CONFDIR}/bq.trigger.n0
	rm ${PREFIX}${CONFDIR}/bq.trigger.n1
	rm ${PREFIX}${SHAREDIR}/README.txt
	rm ${PREFIX}${SHAREDIR}/CHANGELOG.txt

uninstall:	deinstall

clean:
	rm -f bq
	rm -f *.o core *~ a.out *.core

