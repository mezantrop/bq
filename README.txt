BQ (BeaST Quorum) is the simple quorum drive implementation for the FreeBSD
CTL HA and the BeaST Storage system concept.

BeaST Quorum is currently in the early development stage! Use it for testing
purposes only! Do not implement it in production as you can lose your data!

2016.11.25, Mikhail E. Zakharov <zmey20000@yahoo.com>

