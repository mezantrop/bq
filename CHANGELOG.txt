2016.11.25	v1.0	Mikhail Zakharov <zmey20000@yahoo.com>
	* Beast Quorum initial release.

2016.12.07	v1.1	Mikhail Zakharov <zmey20000@yahoo.com>
	* Makefile enhancements: install/deinstall sections;
	* Useful definitions in bq.h;
	* Typographics errors fixing.

2020.08.09	v1.2	Mikhail Zakharov <zmey20000@yahoo.com>
	* Automatic block-device sector size determination for the quorum drive

2020.08.14	v1.3	Mikhail Zakharov <zmey20000@yahoo.com>
	* BQ moved to the sector 65 to avoid possible partitions metadata damage
	
2020.08.15	v1.4	Mikhail Zakharov <zmey20000@yahoo.com>
	* Block numbers fixed in -L output
