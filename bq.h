/* 
  BQ (BeaST Quorum) is the simple quorum drive implementation for the FreeBSD
  CTL HA and the BeaST Storage system concept.

  Best viewed with tabsise: 4
*/

/* BeaST Quorum Header */
typedef struct {
	unsigned char label[2];		/* Label of the quorum drive: "BQ" */
	unsigned char version;		/* Major BQ version number */
	unsigned char n_state[2];	/* Nodes states array: 0/1 - Off/On */
	unsigned char frequency;	/* Heartbeat frequency in seconds */
	unsigned char timeout;		/* Missed heartbeats timeout in seconds */
	unsigned long n_block[2];	/* Timestamps block numbers array */
} BQHEADER;

#define BQ_LOCATION		0x40	/* Sector number to install the BQ header */

#define BQ_LABEL		"BQ"
#define BQ_VERSION		0b00100100
#define BQ_NODE_DEAD	0
#define BQ_NODE_ALIVE	1
#define BQ_HB_FREQUENCY 1
#define BQ_HB_TIMEOUT	10

/* Command-line options */
typedef struct {
	unsigned char I;			/* Install new quorum flag */
	unsigned char L;			/* List quorum data flag */
	unsigned char S;			/* Start quorum flag */
	unsigned char K;			/* Stop (kill) quorum flag */
	unsigned char d;			/* Quorum device flag */
	char *file_bqdev;			/* Quorum device name */
	unsigned long offset;		/* BQ sector number offset */
	unsigned char f;			/* Heartbeat frequency flag */
	int frequency;				/* Heartbeat frequency in seconds */
	unsigned char t;			/* Missed heartbeats timeout flag */
	int timeout;				/* Missed heartbeats timeout in seconds */
	unsigned char n;			/* Current Node flag */
	int thisnode;				/* Current Node ID set by cmd opts */
	int thatnode;				/* Cross Node ID is guessed by logic */
	unsigned char s;			/* Failover service file flag */
	char *file_trigger;			/* Failover service file name */
	unsigned char l;			/* Logfile flag */
	char *file_log;				/* Logfile name */
	unsigned char h;			/* usage() flag */
} OPTS;

#define BQ_FILE_TRIGGER	"/usr/local/etc/bq.trigger"
#define BQ_FILE_LOG		"/var/log/bq.log"

#define TRIGGER_EVENT_ALIVE	"alive"
#define TRIGGER_EVENT_DEAD	"dead"
