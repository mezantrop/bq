/*
 * Copyright (c) 2016, 2020 Mikhail E. Zakharov <zmey20000@yahoo.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    in this position and unchanged.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
  BQ (BeaST Quorum) is the simple quorum drive implementation for the FreeBSD
  CTL HA and the BeaST Storage system concept.

  Best viewed with tabsise: 4
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/disk.h>

#include "bq.h"


char ltime[26];

void usage(void);
int stoi(char *str);
char *log_time();
void write_bqheader(int qfd, BQHEADER bqh, unsigned int sectorsize, 
					unsigned long bq_offset);
BQHEADER read_bqheader(int qfd, unsigned int sectorside, 
					unsigned long bq_offset);
time_t read_bqtimestamp(int qfd, unsigned short int block_id, 
						unsigned int sectorsize, unsigned long bq_offset);
void write_bqtimestamp(int qfd, unsigned short int block_id, time_t timestamp, 
					   unsigned int sectorsize, unsigned long bq_offset);
pid_t run_bqtrigger(char thisnode, char triggernode, 
					char *file_trigger, char *event);

int main(int argc, char *argv[]) {
	BQHEADER bqh = {		/* Beast Quorum header */
		BQ_LABEL,			/* BeaST Quorum label */
		BQ_VERSION,			/* BeaST Quorum version */
		{					/* Node states: 0/1: dead/alive */
			BQ_NODE_DEAD, 	/* Node 0 */
			BQ_NODE_DEAD	/* Node 1 */
		},
		BQ_HB_FREQUENCY,	/* Heartbeat frequency in seconds */
		BQ_HB_TIMEOUT,		/* Missed heartbeats timeout in seconds */
		{1, 2}				/* Node 0, Node 1 timestamp device block numbers */
	};

	OPTS flags = {			/* Default command-line options */
		0,					/* Install new quorum flag */
		0,					/* List quorum data flag */
		0,					/* Start quorum flag */
		0,					/* Stop (kill) quorum flag */
		0,					/* Quorum device flag */
		NULL,				/* Quorum device name */
		BQ_LOCATION,		/* BQ sector number offset */
		1,					/* Heartbeat frequency flag */
		BQ_HB_FREQUENCY,	/* Heartbeat frequency in seconds */
		1,					/* Missed heartbeats timeout flag */
		BQ_HB_TIMEOUT,		/* Missed heartbeats timeout in seconds */
		255,				/* Current Node flag */
		255,				/* Current Node ID set by cmd opts */
		0,					/* Cross Node ID is guessed by logic */
		0,					/* Failover service file flag */
		BQ_FILE_TRIGGER,	/* Failover service file name */
		0,					/* Logfile flag */
		BQ_FILE_LOG,		/* Logfile name */
		0					/* usage() flag */
	};

	int opt;
	int qfd, lfd;
	unsigned int sectorsize = S_BLKSIZE;
	time_t n_time[2], timestamp;
	char *lt;
	int n_prev_state[2] = {-1, -1};		/* Previous nodes states */
	pid_t pid;
	int status;

	/* Get command-line options */
	while ((opt = getopt(argc, argv, "ILSKd:o:f:t:n:s:l:h")) != -1) {
		switch (opt) {
			case 'I':							/* Install new quorum flag */
				flags.I = 1;
				break;
			case 'L':							/* List quorum data flag */
				flags.L = 1;
				break;
			case 'S':							/* Start quorum flag */
				flags.S = 1;
				break;
			case 'K':							/* Stop (kill) quorum flag */
				flags.K = 1;
				break;
			case 'd':							/* Quorum device flag */
				flags.file_bqdev = optarg;
				flags.d = 1;
				break;
			case 'o':
				flags.offset = stoi(optarg);	/* BQ header offset */
				break;
			case 'f':							/* Heartbeat frequency */
				if ((flags.frequency = stoi(optarg)) < 1) {
					warnx("Wrong frequency value: %i", flags.frequency);
					(void)usage();
				}
				flags.f = 1;
				break;
			case 't':							/* Missed heartbeats timeout */
				if ((flags.timeout = stoi(optarg)) < 1) {
					warnx("Wrong timeout value: %i", flags.timeout);
					(void)usage();
				}
				flags.t = 1;
				break;
			case 'n':							/* Node flag */
				flags.thisnode = stoi(optarg);	/* Current Node ID */
				if (flags.thisnode < 0 || flags.thisnode > 1) {
					warnx("Wrong node id value: %i", flags.thisnode);
					(void)usage();
				}
				flags.n = 1;
				break;
			case 's':							/* Service flag */
				flags.file_trigger = optarg;
				flags.s = 1;
				break;
			case 'l':							/* Log file flag */
				flags.file_log = optarg;
				flags.l = 1;
				break;
			case 'h':							/* Usage */
			case '?':
			default:
				(void)usage();
		}
	}
	argc -= optind;
	argv += optind;

	/* Check command-line options consistency */
	if (flags.I + flags.L + flags.S + flags.K != 1) {
		warnx("You must specify one of the commands: -I, -L, -S or -K");
		(void)usage();
	}

	if (!flags.d) {
		warnx("You must specify quorum device with -d key");
		(void)usage();	
	}

	if (flags.I) {
		if (!flags.frequency) {
			warnx("Frequency must be positive");
			(void)usage();
		}
		if (!flags.timeout) {
			warnx("Timeout must be positive");
			(void)usage();
		}
		if (flags.frequency >= flags.timeout) {
			warnx("Timeout must be greater then Frequency");
			(void)usage();
		}
	}

	if (flags.S || flags.K)
		if (flags.n == 255) {
			warnx("You must specify the node id: 0 or 1");
			(void)usage();
		}

	/* Install quorum header ------------------------------------------------ */
	if (flags.I) {
		if ((qfd = open(flags.file_bqdev, O_WRONLY|O_SYNC|O_DIRECT)) == -1)
			err(1, "Unable to open quorum device %s for writing",
				flags.file_bqdev);

		if (ioctl(qfd, DIOCGSECTORSIZE, &sectorsize) == -1)
			err(1, "Unable to get sector-size of the device");

		/* Write quorum header */
		bqh.frequency = flags.frequency;
		bqh.timeout = flags.timeout;
		write_bqheader(qfd, bqh, sectorsize, flags.offset);

		if (close(qfd) == -1)
			err(1, "Unable to close the quorum device");

		printf("BeaST Quorum is installed.\n");

		exit(0);
	}

	/* List quorum data ----------------------------------------------------- */
	if (flags.L) {
		if ((qfd = open(flags.file_bqdev, O_RDONLY|O_DIRECT)) == -1)
			err(1, "Unable to open quorum device %s for reading",
				flags.file_bqdev);

		if (ioctl(qfd, DIOCGSECTORSIZE, &sectorsize) == -1)
			err(1, "Unable to get sector-size of the device");

		/* Read BeaST quorum header */
		bqh = read_bqheader(qfd, sectorsize, flags.offset);

		/* Read Node timestamps */
		n_time[0] = read_bqtimestamp(qfd, bqh.n_block[0], sectorsize, 
									 flags.offset);
		n_time[1] = read_bqtimestamp(qfd, bqh.n_block[1], sectorsize,
									 flags.offset);

		if (close(qfd)== -1)
			err(1, "Unable to close the quorum device");

		printf("BQuorum Label:\t\t%c%c\n", bqh.label[0], bqh.label[1]);
		printf("BQuorum Version:\t%u\n", bqh.version);
		printf("BQuorum block number:\t%lu\n", flags.offset);
		printf("Heartbeat Frequency:\t%u\n", bqh.frequency);
		printf("Heartbeat Timeout:\t%u\n", bqh.timeout);
		printf("Node 0 Current State:\t%u\n", bqh.n_state[0]);
		printf("Node 1 Current State:\t%u\n", bqh.n_state[1]);
		printf("Node 0 Timestamp block:\t%lu\n", bqh.n_block[0] + flags.offset);
		printf("Node 1 Timestamp block:\t%lu\n", bqh.n_block[1] + flags.offset);
		printf("Node 0 Timestamp value:\t%lu\n", n_time[0]);
		printf("Node 1 Timestamp value:\t%lu\n", n_time[1]);

		exit(0);
	}

	/* Start quorum --------------------------------------------------------- */
	if (flags.S) {
		if ((qfd = open(flags.file_bqdev, O_RDWR|O_SYNC|O_DIRECT)) == -1)
			err(1, "Unable to open the quorum device %s for reading",
				flags.file_bqdev);

		if (ioctl(qfd, DIOCGSECTORSIZE, &sectorsize) == -1)
			err(1, "Unable to get sector-size of the device");

		if ((lfd = open(flags.file_log, O_WRONLY|O_APPEND|O_CREAT, 0666)) == -1)
			warn("Unable to open the log-file %s", flags.file_log);

		if (eaccess(flags.file_trigger, X_OK) == -1)
			err(1, "Unable to gain execute rights for the failover routine %s",
				flags.file_trigger);

		/* Become a daemon */
		if (daemon(0, 0) == -1)
			err(1, "Unable to become a daemon");

		if (dup2(lfd, STDOUT_FILENO) == -1) {
			write(lfd, "Unable to duplicate stdout file descriptor\n", 43);
			exit(1);
		}
		if (dup2(lfd, STDERR_FILENO) == -1) {
			write(lfd, "Unable to duplicate stderr file descriptor\n", 43);
			exit(1);
		}
		close(lfd);

		printf("%s: BeaST Quorum v%d at Node %d started operations\n",
			log_time(), bqh.version, flags.thisnode);

		/* Main daemon loop: write own and read cross timestamps */
		for (;;) {
			/* Read BeaST quorum header */
			bqh = read_bqheader(qfd, sectorsize, flags.offset);

			/* Get current timestamp */
			timestamp = time(&timestamp);
			/* Get current time for logging */
			lt = log_time();

			/* Guess cross node number */
			flags.thatnode = flags.thisnode == 0 ? 1 : 0;
			
			/* Read node timestamps */
			n_time[flags.thisnode] = read_bqtimestamp(qfd, 
				bqh.n_block[flags.thisnode], sectorsize, flags.offset);
			n_time[flags.thatnode] = read_bqtimestamp(qfd, 
				bqh.n_block[flags.thatnode], sectorsize, flags.offset);

			/* Set This node active state and timestamp */
			if (!bqh.n_state[flags.thisnode])
				bqh.n_state[flags.thisnode] = BQ_NODE_ALIVE;
			write_bqtimestamp(qfd, bqh.n_block[flags.thisnode], 
							  timestamp, sectorsize, flags.offset);
			if (n_prev_state[flags.thisnode] != bqh.n_state[flags.thisnode]) {
				printf("%s: Node %d is alive\n",  lt, flags.thisnode);
				fflush(stdout);
				pid = run_bqtrigger(flags.thisnode, flags.thisnode,
					flags.file_trigger, TRIGGER_EVENT_ALIVE);
			}

			/* Check That (cross) node status */
			if (timestamp - n_time[flags.thatnode] >= bqh.timeout) {
				/* That node is Dead */
				bqh.n_state[flags.thatnode] = BQ_NODE_DEAD;
				if (n_prev_state[flags.thatnode] != bqh.n_state[flags.thatnode]) {
					printf("%s: Node %d is dead\n", lt, flags.thatnode);
					fflush(stdout);
					pid = run_bqtrigger(flags.thisnode, flags.thatnode,
							flags.file_trigger, TRIGGER_EVENT_DEAD);
				}
			} else {
				/* That node is alive */
				bqh.n_state[flags.thatnode] = BQ_NODE_ALIVE;
				if (n_prev_state[flags.thatnode] != bqh.n_state[flags.thatnode]) {
					printf("%s: Node %d is alive\n", lt, flags.thatnode);
					fflush(stdout);
					pid = run_bqtrigger(flags.thisnode, flags.thatnode,
						flags.file_trigger, TRIGGER_EVENT_ALIVE);
				}
			}

			if (pid > 0) 
				waitpid(pid, &status, WNOHANG); 

			if (n_prev_state[flags.thisnode] != bqh.n_state[flags.thisnode] ||
				n_prev_state[flags.thatnode] != bqh.n_state[flags.thatnode]) {
				write_bqheader(qfd, bqh, sectorsize, flags.offset);

				n_prev_state[flags.thisnode] = bqh.n_state[flags.thisnode];
				n_prev_state[flags.thatnode] = bqh.n_state[flags.thatnode];
			}

			sleep(bqh.frequency);
		}

		if (close(qfd)== -1)
			err(1, "Unable to close the quorum device");

		exit(0);
	}

	/* Stop (kill) quorum flag ---------------------------------------------- */
	if (flags.K) {
		printf("Not implemented yet, sorry\n");
		exit(0);
	}
}

/* -------------------------------------------------------------------------- */
void usage(void) {
	printf("Usage:\n\
bq -I -d /dev/devX [-o 64] [-f 1] [-t 10]\n\
\t-I\tInstall BeaST Quorum to the shared block device.\n\
\t-d\tdevice to install.\n\
\t-o\tBQ header location as offset from the beginning of the disk\n\
\t\tin sectors. Default: 64\n\
\t-f n\thearbeat frequency in seconds. Default: 1\n\
\t-t k\theartbeat timeout in seconds. Default: 10\n\
\n\
bq -L -d /dev/devX [-o 64]\n\
\t-L\tList BeaST Quorum information.\n\
\t-d\tquorum device to list.\n\
\t-o\tBQ header location as offset from the beginning of the disk\n\
\t\tin sectors. Default: 64\n\
\n\
bq -S -d /dev/devX -n 0|1 [-o 64] [-s /path/to/bq.trigger] [-l /path/to/bq.log]\n\
\t-S\tStart BeaST Quorum instance.\n\
\t-d\tquorum device to use.\n\
\t-n 0|1\tthis node number.\n\
\t-o\tBQ header location as offset from the beginning of the disk\n\
\t\tin sectors. Default: 64\n\
\t-s /path/to/bq.trigger command to run on node alive/dead event.\n\
\t\tDefault path is /usr/local/etc/bq.trigger\n\
\t-l /path/to/bq.log full path to log-file.\n\
\t\tDefault path is /usr/local/var/log/bq.log\n\
\n\
bq -K -d /dev/devX -n 0|1 [-o 64]\n\
\n\
\tNot implemented yet.\n\
\n\
\t-K\tKill (stop) BeaST Quorum instance.\n\
\t-d\tquorum device to use.\n\
\t-n 0|1\tthis node number.\n\
\t-o\tBQ header location as offset from the beginning of the disk\n\
\t\tin sectors. Default: 64\n");
	exit(1);
}

/* String to integer -------------------------------------------------------- */
int stoi(char *str) {
	int	i;

	i = (int)strtol(str, (char **)NULL, 10);
	if (i == 0 && errno == EINVAL) {
		err(1, "Unable to convert: %s to integer", str);
		usage();
	}
	return i;
}

/* Write BeaST header to the device ----------------------------------------- */
void write_bqheader(int qfd, BQHEADER bqh, unsigned int sectorsize, 
					unsigned long bq_offset) {

	unsigned char *buf;
	ssize_t bytes;
	char *lt;

	lt = log_time();

	if ((buf = malloc(sectorsize)) == NULL)
		err(1, "%s Unable to allocate memory for the R/W buffer", lt);
	memset(buf, 0, sectorsize);

	memmove(buf, &bqh, sizeof(bqh));

	if (lseek(qfd, bq_offset * sectorsize, SEEK_SET) == -1)
		err(1, "%s Error locating quorum header at block: %i", lt, BQ_LOCATION);

	bytes = write(qfd, buf, sectorsize);
	if (bytes == -1)
		err(1, "%s Unable to install the quorum header", lt);
	if (bytes < sectorsize)
		err(1, "%s Only %ld of %i of the quorum header is written",
			lt, bytes, sectorsize);

	free(buf);
}

/* Read BeaST header from the device ---------------------------------------- */
BQHEADER read_bqheader(int qfd, unsigned int sectorsize, 
					   unsigned long bq_offset) {

	unsigned char *buf;
	ssize_t bytes, bqsize;
	BQHEADER bqh;
	char *lt;

	lt = log_time();

	if ((buf = malloc(sectorsize)) == NULL)
		err(1, "%s Unable to allocate memory for the R/W buffer", lt);
	memset(buf, 0, sectorsize);

	if (lseek(qfd, bq_offset * sectorsize, SEEK_SET) == -1)
		err(1, "%s Error locating quorum header at block: %i", lt, BQ_LOCATION);
	
	bytes = read(qfd, buf, sectorsize);
	if (bytes == -1)
		err(1, "%s Unable to read the quorum device", lt);
	if (bytes < (bqsize = sizeof(BQHEADER)))
		errx(1, "%s Read %ld only bytes from the quorum device", lt, bytes);

	memmove(&bqh, buf, bqsize);

	free(buf);

	return bqh;
}

/* Read node timestamp from the device -------------------------------------- */
time_t read_bqtimestamp(int qfd, unsigned short int nblock, 
						unsigned int sectorsize, unsigned long bq_offset) {
	unsigned char *buf;
	ssize_t bytes;
	time_t tstamp;
	char *lt;

	lt = log_time();

	if ((buf = malloc(sectorsize)) == NULL)
		err(1, "%s Unable to allocate memory for the R/W buffer", lt);
	memset(buf, 0, sectorsize);

	if (lseek(qfd, bq_offset * sectorsize + nblock * sectorsize, 
		      SEEK_SET) == -1)
		err(1, "%s Error locating node timestamp at block: %i", 
			lt, BQ_LOCATION + nblock);

	bytes = read(qfd, buf, sectorsize);
	if (bytes == -1)
		err(1, "%s Error reading node timestam at block: %i", lt, nblock);
	if (bytes < sectorsize)
		err(1, "%s Partial node timestam read at block: %i", lt, nblock);

	memmove(&tstamp, buf, sizeof(tstamp));

	free(buf);

	return tstamp;
}

/* Write node timestamp to the device --------------------------------------- */
void write_bqtimestamp(int qfd, unsigned short int nblock, time_t timestamp,
		       		   unsigned int sectorsize, unsigned long bq_offset) {

	unsigned char *buf;
	ssize_t bytes;
	time_t tstamp;
	char *lt;

	lt = log_time();
	tstamp = timestamp;

	if ((buf = malloc(sectorsize)) == NULL)
		err(1, "%s Unable to allocate memory for the R/W buffer", lt);
		
	memset(buf, 0, sectorsize);

	memmove(buf, &tstamp, sizeof(tstamp));

	if (lseek(qfd, bq_offset * sectorsize + nblock * sectorsize, SEEK_SET) == -1)
		err(1, "%s Error locating node timestamp at block: %i",
			lt, BQ_LOCATION + nblock);

	bytes = write(qfd, buf, sectorsize);
	if (bytes == -1)
		err(1, "%s Error writing node timestamp at block: %i", lt, nblock);
	if (bytes < sectorsize)
		err(1, "%s Partial timestamp written at block: %i", lt, nblock);

	free(buf);
}

/* Format time for log messages --------------------------------------------- */
char *log_time() {
	struct tm* lt;
	time_t timestamp;

	time(&timestamp);
	lt = localtime(&timestamp);
	strftime(ltime, 26, "%Y-%m-%d %H:%M:%S", lt);

	return ltime;
}

/* Run external command/script on the event --------------------------------- */
pid_t run_bqtrigger(char thisnode, char triggernode, char *file_trigger, 
					char *event) {

	pid_t pid;
	char this_node[2], trigger_node[2];
	char *lt;

	lt = log_time();

	snprintf(this_node, 2, "%d", thisnode);
	snprintf(trigger_node, 2, "%d", triggernode);

	char *argv[] = {file_trigger, this_node, trigger_node, event, 0};

	if ((pid = fork()) == -1) 
		err(1, "%s Unable to fork a new proccess", lt);

	if (pid == 0) {
		printf("%s Executing trigger: %s %s %s %s",
			lt, argv[0], argv[1], argv[2], argv[3]);

		if (execve(argv[0], &argv[0], NULL) == -1) {
			/* execve() exits only on error so: */
			err(1, "%s Unable to execute %s %s %s %s",
				lt, argv[0], argv[1], argv[2], argv[3]);
		}
	}

	return pid;
}
